package Week04Redo;

import com.google.gson.Gson;

public class jon {

    static class Student{
        String name;
        String age;
        public Student(String name,String age){
            this.name = name;
            this.age = age;

        }
    }
    public static void jon(String[] args) {

        String jsonText = "{\"name\":\"Harry Potter\",\"age\":\"11\"}";

        com.google.gson.Gson gson = new Gson();

        Student student = gson.fromJson( jsonText , Student.class);

        System.out.println("Name: " + student.name +  "\nAge: " + student.age);

    }
}
