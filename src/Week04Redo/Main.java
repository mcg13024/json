package Week04Redo;

import com.google.gson.Gson;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        String jsonText = "{\"name\":\"Harry\",\"age\":\"11\"}";
        Gson gson = new Gson();
        Main.Student student = (Main.Student)gson.fromJson(jsonText, Main.Student.class);
        System.out.println(student.name + " " + student.age);
    }

    static class Student {
        String name;
        String age;

        public Student(String name, String age) {
            this.name = name;
            this.age = age;
        }
    }
}
